{-# LANGUAGE GADTs, RankNTypes, TypeFamilies, TypeOperators #-}

module Data.LogicGraph.Types where

data U
data I
data J
data L
data T
data R
data G
data Edge a
data Vert a
data Node a
data Pair a
data Grph a
data ColN a
data ColC a
data a :* b

type Ports a = [Elem I R (Pair a)]

data Elem a b c where

  Unit ::                                                                                     Elem a b c

  Docu :: a                                                                                -> Elem U U (Vert a)

  Rfer ::                                                                                     Elem R R a

  Iden :: Integer -> Elem a b c                                                            -> Elem (IdenI a b) (IdenL a b) c

  Labl :: NotRef a => String -> Elem a U b                                                 -> Elem a L b

  Ness :: [Elem I R (Vert a)] -> [Elem I R (Vert a)]                                       -> Elem U U (Edge a)
  Witn :: [Elem I R (Vert a)] -> [Elem I R (Vert a)]                                       -> Elem U U (Edge a)
  Toka :: [Elem I R (Vert a)] -> [Elem I R (Vert a)]                                       -> Elem U U (Edge a)
  Comm :: [Elem I R (Vert a)]                                                              -> Elem U U (Edge a)

  ColN ::  Elem I T (Vert a)  ->  Elem I T (Vert a)                                        -> Elem U U (ColN a)
  ColC ::  Elem I T (ColN a)  -> [((Int,Int),[(Int,Int)])]                                 -> Elem U U (ColC a)

  Libr :: Ports a                                                                          -> Elem U U (Node a)
  Ordr :: Ports a                                                                          -> Elem U U (Node a)
  AndW :: Ports a                                                                          -> Elem U U (Node a)
  AndN :: Ports a                                                                          -> Elem U U (Node a)

  Pair :: Elem I R (Vert b) -> Elem I R (Vert b)                                           -> Elem U U (Pair a)
  Tree :: Elem I b a -> Elem I c a                                                         -> Elem I T       a

  --Tree :: Elem I b a -> Elem I c a                                                         -> Elem I (b :* c)       a

  Grph :: Elem I b (Vert a) -> Elem I c (Edge a) -> Elem I d (Node a) -> Elem I e (Grph a) -> Elem G U (Grph a)

  Subg :: IsId b => Elem b R (Grph a) -> [Int] -> [Int] -> [Int]                           -> Elem U U (Grph a)

type family Tree a b where

type family IdenI a b where
  IdenI G b = J
  IdenI a b = I

type family IdenL a b where
  IdenL R R = R
  IdenL U L = L
  IdenL U U = U

class    NotRef a where
instance NotRef U where
instance NotRef I where
instance NotRef L where
instance NotRef T where
instance (NotRef a, NotRef b) => NotRef (a :* b) where

class IsId a where
instance IsId I where
instance IsId J









