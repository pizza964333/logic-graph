{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, TypeFamilies, GADTs, OverloadedStrings  #-}

module Data.LogicGraph.Dot where

import Data.LogicGraph.Types
import qualified Data.GraphViz as G
import Data.GraphViz hiding (toDot)
import Data.GraphViz.Types hiding (toDot)
import Data.GraphViz.Types.Canonical
import Data.GraphViz.Attributes
import Data.GraphViz.Attributes.Complete
import System.IO.Unsafe
import System.Random

data Dot = Dot String

class ToDot a b where
 toDot :: a-> b

instance ToDot (Elem I U (Edge a)) (DotEdge Int) where
  toDot (Iden n (Witn [Iden a Rfer] [Iden b Rfer])) = DotEdge b a [ArrowHead (AType [(ArrMod OpenArrow BothSides,Diamond)])]

instance ToDot (Elem I U (Edge a)) (IO ([DotNode Int],[DotEdge Int])) where
  toDot (Iden n (Witn a b)) = do
    mid1 <- randomRIO (10^7,10^8 :: Int)
    mid2 <- randomRIO (10^7,10^8 :: Int)
    let listA = map (\(Iden a Rfer) -> DotEdge mid1 a [ArrowHead (AType [(ArrMod OpenArrow BothSides,Diamond)])]) a
    let listB = map (\(Iden b Rfer) -> DotEdge b mid2 [ArrowHead (AType [(ArrMod OpenArrow BothSides,NoArrow)])]) b
    let node1  = DotNode mid1 [Shape PlainText,Label (StrLabel ""),Width 0,Height 0]
    let node2  = DotNode mid2 [Shape PlainText,Label (StrLabel ""),Width 0,Height 0]
    let edge   = DotEdge mid2 mid1 [ArrowHead (AType [(ArrMod OpenArrow BothSides,NoArrow)])]
    return $ ([node1,node2],listA ++ listB ++ [edge])

-- instance ToDot (Elem U U (Node a)) 

test01 :: DotEdge Int
test01 = toDot $ Iden 1 (Witn [Iden 2 Rfer] [Iden 3 Rfer])

test03 :: ([DotNode Int],[DotEdge Int])
test03 = unsafePerformIO $ toDot $ Iden 4 (Witn [Iden 3 Rfer, Iden 4 Rfer, Iden 5 Rfer] [Iden 6 Rfer, Iden 7 Rfer, Iden 8 Rfer])

(aa,bb) = test03

test02 = DotGraph False True Nothing $ DotStmts [] [] (aa) ([test01] ++ bb)

main = runGraphviz test02 Png "/tmp/1.png"




